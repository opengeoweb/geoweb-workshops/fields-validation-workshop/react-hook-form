//  1. npm install
//  2. nx serve react-hook-form-demo

// 1. handleSubmit
// 2. register
// 3. required
// 4. errors
// 5. minLength
// 6. watch pizza
// 7. validate drinks
// 8. trigger age
// 9. async username
import * as React from 'react';
import { useForm } from 'react-hook-form';

import './app.css';

const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

const onSubmit = (data) => {
  console.log('submit', data);
};

const canBuyBeer = (drinks, age) => {
  if (drinks === 'beer') {
    return age >= 18;
  }

  return true;
};

const isUniqueUsername = async (name) => {
  await sleep(1000);

  if (name === 'test') {
    return false;
  }
  return true;
};

export const App = () => {
  const { handleSubmit, register, errors, watch, trigger } = useForm();

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <h1>KNMI KISS Demo</h1>
      <label>Username</label>
      <input
        type="text"
        name="username"
        ref={register({
          required: true,
          minLength: 3,
          validate: { isUniqueUsername },
        })}
      />

      {errors.username && errors.username.type === 'required' && (
        <p>this field is required</p>
      )}

      {errors.username && errors.username.type === 'minLength' && (
        <p>this field has a minLength of 3</p>
      )}

      {errors.username && errors.username.type === 'isUniqueUsername' && (
        <p>username should be unique</p>
      )}

      <label>Age</label>
      <input
        type="number"
        name="age"
        ref={register({ required: true })}
        onChange={() => trigger('drinks')}
      />

      {errors.age && errors.age.type === 'required' && (
        <p>this field is required</p>
      )}

      <label>Food</label>
      <select name="food" ref={register}>
        <option value="">Select your food</option>
        <option value="pizza">Pizza</option>
        <option value="fries">Fries</option>
        <option value="kapsalon">Kapsalon</option>
      </select>

      {watch('food') === 'pizza' && (
        <select name="toppings" ref={register}>
          <option value="">Select your food</option>
          <option value="cheese">cheese</option>
          <option value="ananas">ananas</option>
          <option value="olives">olives</option>
        </select>
      )}

      <label>Drinks</label>
      <select
        name="drinks"
        ref={register({
          validate: {
            canBuyBeer: (val) => canBuyBeer(val, watch('age')),
          },
        })}
      >
        <option value="">Select a drink</option>
        <option value="cola">Cola</option>
        <option value="water">Water</option>
        <option value="beer">Beer</option>
      </select>

      {errors.drinks && errors.drinks.type === 'canBuyBeer' && (
        <p>should be 18 </p>
      )}

      <input type="submit" />
    </form>
  );
};

export default App;
