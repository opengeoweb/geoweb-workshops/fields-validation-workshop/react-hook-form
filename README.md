# Front-end validation using react-hook-form

Workshop created by the KNMI GeoWeb development team. 
It show how to make a basic front-end form validation using react-hook-form.

## Run the application

Run `nx serve` to run the application and navigate to the localhost (i.g. http://localhost:4200/ ). The app will automatically reload if you change any of the source files.

## CodeSandbox

It is possible to try out the application on [CodeSandbox](https://codesandbox.io/s/interesting-pond-kbveh?file=/src/App.js)

## Documentation
More documentation available on [wiki](https://agora.fmi.fi/display/GEOWEB/Events).
